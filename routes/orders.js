const express = require('express');
const OrderController = require('./../controllers/orders');
const auth = require('./../auth');
const router = express.Router();

// Router to create order
router.post('/createOrder', auth.verify, OrderController.createOrder);

// Router to retrieve orders
router.get('/getOrders', auth.verify, OrderController.getOrders);

// Router to retrieve all orders/s
router.get('/getAllOrders', auth.verify, auth.verifyAdmin, (req, res) => {
	OrderController.getAllOrders().then(resultFromController => res.send(resultFromController));
});

// Router to delete specific order
router.delete('/:orderId', auth.verify, (req, res) => {
	OrderController.deleteOrder(req.params).then(resultFromController => res.send(resultFromController));
});

// Router to checkout user's cart
router.put('/checkout', auth.verify, (req, res) => {
	OrderController.checkoutOrders(req.user).then(resultFromController => res.send(resultFromController));
});

// Router to retrieve user's orders
router.get('/history', auth.verify, (req, res) => {
	OrderController.orderHistory(req.user).then(resultFromController => res.send(resultFromController));
});

module.exports = router;