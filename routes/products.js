
const express = require('express');
const ProductController = require('./../controllers/products');
const auth = require('./../auth');

// [SECTION] Routing Component
const router = express.Router();

// [SECTION] Routes

// Router to add product
router.post('/add', auth.verify, auth.verifyAdmin, (req, res) => {
	ProductController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
});

// Controller to retrieve all products
router.get('/all', (req, res) => {
	ProductController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Router to retrieve all active products
router.get('/active', (req, res) => {
	ProductController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Router to retrieve specific product
router.get('/:productId', (req, res) => {
	ProductController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Router to update a product
router.put('/:productId', auth.verify, auth.verifyAdmin, (req, res) => {
	ProductController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Router to archive a product
router.put('/:productId/archive', auth.verify, auth.verifyAdmin, (req, res) => {
	ProductController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Router to activate a product
router.put('/:productId/activate', auth.verify, auth.verifyAdmin, (req, res) => {
	ProductController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
