const express = require('express');
const UserController = require('./../controllers/users');
const auth = require('./../auth');
const router = express.Router();

// Router to register a user
router.post('/register', (req, res) => {
	UserController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Router to login user
router.post('/login', (req, res) => {
	UserController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Router to set user as admin
router.put("/:userId/setAsAdmin", auth.verify, (req,res) => {
let data = {
	userId: auth.decode(req.headers.authorization).id,
	isAdmin: auth.decode(req.headers.authorization).isAdmin
}
	UserController.setAsAdmin(req.params, data).then(resultFromController => res.send (resultFromController));
});

// Router to set admin as user
router.put("/:userId/setAsUser", auth.verify, (req,res) => {
let data = {
	userId: auth.decode(req.headers.authorization).id,
	isAdmin: auth.decode(req.headers.authorization).isAdmin
}
	UserController.setAsUser(req.params, data).then(resultFromController => res.send (resultFromController));
});

// Router to delete user
router.delete("/:userId/delete", auth.verify, (req,res) => {
	UserController.deleteUser(req.params.userId).then(result => res.send(result))
});

// Router to get all users
router.get("/all", auth.verify, (req,res) => {
	UserController.getAllUsers().then(result => res.send(result))
})

// Router to get user's profile
router.get('/details', auth.verify, (req, res) => {
	UserController.getProfile(req.user.id).then(resultFromController => res.send(resultFromController));
});

module.exports = router;