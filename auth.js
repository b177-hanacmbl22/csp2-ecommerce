const jwt = require('jsonwebtoken');
const secret = 'CSP2-Ecommerce';

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		username: user.username,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
};

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if (typeof token !== 'undefined') {
		token = token.slice(7, token.length);

		jwt.verify(token, secret, (error, decodedToken) => {
			if (error) {
				return res.send({
					auth: 'Failed',
					message: error.message
				});
			} else {
				req.user = decodedToken;
				
				next();
			}
		});
	} else {
		return res.send({auth: 'Failed. No token'});
	}
};

// Token decryption
module.exports.decode = (token) => {
	
	if(typeof token != "undefined"){
		
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) =>{
			if(err){
				return null;
			}
			else{
				return jwt.decode(token, {complete:true}).payload
			}
		})
	}

	else{
		return null;
	}
}

module.exports.verifyAdmin = (req, res, next) => {
	if (req.user.isAdmin) {
		next();
	} else {
		return res.send({
			auth: 'Failed',
			message: 'Not allowed in this access'
		});
	}
}