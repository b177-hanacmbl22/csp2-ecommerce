const User = require('./../models/User');
const Product = require('./../models/Product');
const bcrypt = require('bcrypt');
const auth = require('./../auth');

// Controller to register a user
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		username: reqBody.username,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if (user) {
			return user;
		} else {
			return false;
		}
	});
};

// Controller to login user
module.exports.loginUser = (reqBody) => {
	return User.findOne({username: reqBody.username}).then(result => {
		if (result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {
				return {accessToken: auth.createAccessToken(result.toObject())};
			} else {
				return false;
			}
		}
	});
};

// Controller to set user as admin
module.exports.setAsAdmin = (reqParams, reqBody) => {
if(reqBody.isAdmin) {
	
	return User.findByIdAndUpdate(reqParams.userId, {isAdmin: true}).then((result, error) => {
		if(error){
			return (false);
		}
		else{
			return (true);		
		}
	})
}
else {
	return Promise.resolve(false);
}
};

// Controller to set admin as user
module.exports.setAsUser = (reqParams, reqBody) => {
if(reqBody.isAdmin) {
	
	return User.findByIdAndUpdate(reqParams.userId, {isAdmin: false}).then((result, error) => {
		if(error){
			return (false);
		}
		else{
			return (true);		
		}
	})
}
else {
	return Promise.resolve(false);
}
};

// Controller to delete user
module.exports.deleteUser = (params) => {
return User.findByIdAndDelete(params)
.then((result, error) => {
	if(error){
		return error;
	} else {
		return true;
	}
})
};

// Controller to get all users
module.exports.getAllUsers = () => {
return User.find()
.then(result => {
	return result;
})	
};

// Controller to get user's profile
module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		result.password = '';
		return result;
	});
};