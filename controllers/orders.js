const User = require('./../models/User');
const Product = require('./../models/Product');
const Order = require('./../models/Order');
const auth = require('./../auth');

// Controller to create order
module.exports.createOrder = async (req, res) => {
	if (req.user.isAdmin) {
		return false;
	}

	let isOrderAdded = await Product.findById(req.body.productId).then(product => {
		let newOrder = new Order({
			userId: req.user.id,
			totalAmount: req.body.quantity * product.price
		});

		newOrder.products.push(req.body);

		return newOrder.save().then(product => true).catch(error => error.message);

		if (isOrderAdded !== true) {
			return res.send({message: isOrderAdded});
		}
	});

	if (isOrderAdded) {
		return res.send({message: 'Order Added'});
	}
};

// Controller to retrieve orders
module.exports.getOrders = (req, res) => {
	let user = req.user.id;

	return Order.find({userId: user, isPaid: false}).then(result => {
		return res.send(result);
	})
};

// Controller to retrieve all orders/s
module.exports.getAllOrders = () => {
	return Order.find({}).then((result, error) => {
		if (error) {
			return false;
		} else {
			return result;
		}
	});
};

// Controller to delete specific order
module.exports.deleteOrder = (reqParams) => {
	return Order.findByIdAndRemove(reqParams.orderId).then((result, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};

// Controller to checkout user's cart
module.exports.checkoutOrders = (reqParams) => {
	return Order.updateMany({userId: reqParams.id}, {isPaid: true}).then((result, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};

// Controller to retrieve user's orders
module.exports.orderHistory = (reqUser) => {
	return Order.find({userId: reqUser.id, isPaid: true}).then((result, error) => {
		if (error) {
			return false;
		} else {
			return result;
		}
	});
};