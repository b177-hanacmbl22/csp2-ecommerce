const Product = require('./../models/Product');

// Controller to add product
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		imageLink : reqBody.imageLink
	});

	return newProduct.save().then((product, error) => {
		if (error) {
			return {message: 'Adding of product failed'};
		} else {
			return product;
		}
	});
};

// Controller to retrieve all products
module.exports.getAllProducts = () => {
	return Product.find({}).then((result, error) => {
		return result;
	});
};

// Controller to retrieve all active products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
};

// Controller to retrieve specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
};

// Controller to update a product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		imageLink: reqBody.imageLink
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};

// Controller to archive a product
module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
};

// Controller to activate a product
module.exports.activateProduct = (reqParams) => {
	let updateActiveField = {
		isActive: true
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
};