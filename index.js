const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/users');
const productRoutes = require('./routes/products');
const orderRoutes = require('./routes/orders');

const app = express();
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({extended:true}));

mongoose.connect(
	"mongodb+srv://admin:admin@wdc028-course-booking.jgm9m.mongodb.net/csp3-ecommerce?retryWrites=true&w=majority",{
	useNewUrlParser :true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);

app.listen(process.env.PORT || 4000, () =>{
	console.log(`API is now online on port ${process.env.PORT || 4000 }`)
});

